import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import LoginScreen from './Components/LoginScreen'
import EmployeeDetails from './Components/EmployeeDetails'

const StackNavigatorScreen = StackNavigator({
    LoginScreen: { screen: LoginScreen},
    EmployeeDetails: {screen: EmployeeDetails},
})

export default StackNavigatorScreen
