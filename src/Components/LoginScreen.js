import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  TextInput,
  Platform,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
var loginData =
{
  "username": "hruday@gmail.com",
  "password": "hruday123"
}

class LoginScreen extends React.PureComponent{

  // Default Navigation Of Screen
  static navigationOptions = ({ navigation }) => {
    return {
      title: ``,
      header:null
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }

  componentDidMount() {

  }

  render() {
    return(
      <View style={{
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'center'}}>
        <View style={{
          alignItems: "center",
          justifyContent: 'center'
        }}>
        <Image style={{
          height: 200,
          width: 300,
          resizeMode: 'contain'
        }}
        source={require('../images/unnamed.png')}
        >
        </Image>
        </View>
        <TextInput
        style={{
          height: 50,
          borderColor: 'gray',
          marginLeft: 20,
          marginRight: 20,
          marginTop: 20 }}
          ref = {(input) => this.emailInput = input}
          placeholder='Email'
          borderBottomColor='rgba(0,0,0,0.2)'
          borderBottomWidth={1}
          onChangeText={(text) =>{
            this.setState({username: text.toLowerCase()})
          }}
          onSubmitEditing={(event) => {
            if (LoginScreen.isEmailValid(event.nativeEvent.text)) {
              Alert.alert(
                'Request failed!',
                'Email is Invalid',
                [
                  {text: 'OK', onPress: () => {
                    this.emailInput.focus()
                  }},
                ],
                { cancelable: false }
              )
            } else {
              this.passwordInput.focus()
            }
          }}
          />
          <TextInput
          style={{
            height: 50,
            borderColor: 'gray',
            marginLeft: 20,
            marginRight: 20 }}
            ref = {(input) => this.passwordInput = input}
            placeholder='Password'
            borderBottomColor='rgba(0,0,0,0.2)'
            borderBottomWidth={1}
            onChangeText={(text) =>{
              this.setState({password: text.toLowerCase()})
            }}
            />
            <TouchableOpacity style={{
              alignItems: 'center',
              minHeight: 60,
              borderRadius: 10,
              width: 300,
              marginTop: 50,
              backgroundColor: '#00B7E1',
              justifyContent: 'center',
              alignSelf: 'center'
            }}
            onPress={() => this.Login()}
            >
            <Text style={{
              color: 'white',
              fontSize: 20,
            }}> Log in </Text>
            </TouchableOpacity>
            </View>
          )
        }

        //Validate The Given Email
        static isEmailValid(email) {
          let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
          return reg.test(email) == 0;
        }

        Login(){
          if (this.state.username.length > 0 && this.state.password.length > 0){
            if((this.state.username == loginData.username) && (this.state.password == loginData.password)){
              this.props.navigation.navigate('EmployeeDetails')
            }else{
              Alert.alert(
                `Try Again!!!`,
                `Please enter correct details`,
                [
                  {text: 'OK', onPress: () => console.log("not found")},
                ],
                { cancelable: false }
              )
            }
          }else{
            Alert.alert(
              'Required Email and Password',
              "Email and Password should not be empty",
              [
                {text: 'OK', onPress: () => 'OK PRESSED', style: 'cancel'},
              ],
              { cancelable: false }
            )
          }
        }
      }
      export default LoginScreen;
