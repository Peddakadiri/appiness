import React, { Component } from 'react';
import {
  View,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
var EmployeeData = [{
  "id": 1,
  "name": "test1",
  "age": "11",
  "gender": "male",
  "email": "test1@gmail.com",
  "phoneNo": "9415346313"
}, {
  "id": 2,
  "name": "test2",
  "age": "12",
  "gender": "male",
  "email": "test2@gmail.com",
  "phoneNo": "9415346314"
}, {
  "id": 3,
  "name": "test3",
  "age": "13",
  "gender": "male",
  "email": "test3@gmail.com",
  "phoneNo": "9415346315"
}, {
  "id": 4,
  "name": "test4",
  "age": "14",
  "gender": "male",
  "email": "test4@gmail.com",
  "phoneNo": "9415346316"
}, {
  "id": 5,
  "name": "test5",
  "age": "15",
  "gender": "male",
  "email": "test5@gmail.com",
  "phoneNo": "9415346317"
}, {
  "id": 6,
  "name": "test6",
  "age": "16",
  "gender": "male",
  "email": "test6@gmail.com",
  "phoneNo": "9415346318"
}]

class EmployeeDataTable extends Component {

  //Initialize the State
  constructor(props){
    super(props);
  }

  //Invoked before component is mounted
  componentWillMount() {

  }

  //Invoked immediately after a component is mounted
  componentDidMount() {
  }

  //Invoked immediately before a component is unmounted and destroyed
  componentWillUnmount() {
  }

  //Main Content to display, which encapsulates all the components in it.
  render() {
    return (
      <View style={{
        margin: 5,
        padding: 10,
        elevation: 4,
        borderRadius: 10
      }}>
      {this._renderViewCell('Name:', this.props.item.name)}
      {this._renderViewCell('Age:', this.props.item.age)}
      {this._renderViewCell('Gender:', this.props.item.gender)}
      {this._renderViewCell('Email:', this.props.item.email)}
      {this._renderViewCell('Mobile:', this.props.item.phoneNo)}
      </View>
    )
  }

  _renderViewCell(key, value){
    return(
      <View style={{
        flexDirection: 'row',
      }}>
      <Text style={{
        fontSize: 20,
        flex: 0.5
      }}> {key}  </Text>
      <Text style={{
        fontSize: 20,
        flex: 0.5
      }}> {value} </Text>
      </View>
    )
  }
}


class EmployeeDetails extends React.PureComponent{

  // //Default Navigation Of Screen
  static navigationOptions = {
    headerTitle:'Employee Details',
    headerTitleStyle: {
      color:'#22A7F0'
    },
    headerStyle: {
      backgroundColor:'white'
    },
    headerTintColor: 'black',
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {

  }

  render() {
    return(
      <View style={{backgroundColor: 'white', flex: 1}}>
      {this._renderDealsList()}
      </View>
    )
  }

  _renderDealsList(){
    return(
      <FlatList
      data={EmployeeData}
      renderItem = {({item, index}) => {
        return (
          <EmployeeDataTable
          item={item} index={index} navigation={this.props.navigation}
          >
          </EmployeeDataTable>
        );
      }}
      keyExtractor={item => item._id}>
      </FlatList>
    )
  }
}


export default EmployeeDetails;
