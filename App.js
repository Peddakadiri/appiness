import React from 'react'

import NavigatorScreen from './src/NavigatorScreen'

export default function App() {
  return <NavigatorScreen />
}
